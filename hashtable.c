#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#define MAX_SIZE 100
#define DELETED "deleted"

typedef struct Item {
    int chave;
    char* valor;
} Item;

typedef struct HashTable {
    Item** items;
    int size;
} HashTable;

HashTable* create_table(int size) {
    HashTable* table = (HashTable*) malloc(sizeof(HashTable));
    table->items = (Item**) calloc(size, sizeof(Item*));
    table->size = size;

    for (int i = 0; i < table->size; i++)
        table->items[i] = NULL;

    return table;
}

int string_hash(char * s) {
	char c;
	int p = 31, m = 97;
	int hash_value = 0, p_pow = 1;
	
	while (c = *s++){
		hash_value = (hash_value + (c - 'a' + 1) * p_pow) % m;
		p_pow = (p_pow * p) % m;
	}
	return(hash_value);
}

Item * create_item(int chave, char * dado) {
	Item * item = (Item*) malloc(sizeof(Item));
	item->chave = chave;
	item->valor = dado;
}

int isDeleted(Item * item) {
	return strcmp(item->valor, DELETED);
}

void insert(char * dado, HashTable * ht) {
	int chave = string_hash(dado);
	Item * item = create_item(chave, dado);
	for (int i = chave; i < ht->size; i++) {
		if (ht->items[i] == NULL || isDeleted(ht->items[i]) == 0) {
			ht->items[i] = item;
			return;
		}
	}
}

int search(char * dado, HashTable * ht) {
	int chave = string_hash(dado);
	for (int i = chave; i < ht->size; i++) {
		if (ht->items[i]->valor == dado)
			return i;
	}
}

void remove_item(char * dado, HashTable * ht) {
	int chave = string_hash(dado);
	ht->items[search(dado, ht)]->valor = DELETED;
}


int main(){
	char s[255];
	HashTable * ht = create_table(MAX_SIZE);
	insert("lucas", ht);
	insert("alex kutzke", ht);
	insert("foi mais facil do que parecia", ht);
	remove_item("alex kutzke", ht);
	for (int i = 0; i < ht->size; i++) {
		if (ht->items[i] != NULL && isDeleted(ht->items[i]) != 0)
			printf("Chave: %d\nValor: %s\n", ht->items[i]->chave, ht->items[i]->valor);	
	}
	
	free(ht);
}
